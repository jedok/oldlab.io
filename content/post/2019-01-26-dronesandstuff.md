---
title: Drones in Next Generation Networks
date: 2019-01-26
bigimg: [{src: "../../img/posts/drones/dronebg.jpg", desc: "Military Drones on display!"}]
tags: ["drones", "university", "computer", "science", "drone", "deployment"]
summary: Unmanned Hierarchical Networks!
image: /img/posts/drones/dronebg-eye.jpg
comments: true
---

## Background
Since the introduction of affordable drones, the number of users utilizing drones has risen. Mainly for their high mobility and support to complete aerial tasks. Now that 5G is just around the corner, researchers are focusing on new methods to optimise the placement of drones in air and on ground while stationed. Thus improving cellular connectivity and reducing the average latency between aerial user equipment and terrestrial equipment.

## [A Tutorial on UAVs for Wireless Networks: Applications, Challenges, and Open Problems](https://arxiv.org/pdf/1803.00680.pdf)
### Introduction

### Types of UAVs
![Class of drones](/img/posts/drones/typesofdrones.jpg)

In paper [1] the authors have created a tree graph that branch into the sub-classes of UAVs.

![Class of drones](/img/posts/drones/classofdrones.jpg)

Among the class of UAVs, I have created a small table to show rotary-wing drone specifications.

### [Beyond 5G with UAVs foundations of a 3D Wireless cellular networks](https://arxiv.org/pdf/1805.06532.pdf)
### Introduction
- The use of massive drones will have significant impact on wireless networking.
- Applications include thermal imaging, package Delivery, Aerial base stations.
- In this paper the authors consider low altitude user equipment devices in a 3D area served by a High Altitude platform access points consisting of UAVs such as blimps.

### Background
- Previous work ignore the existence of flying drone user equipment (UE).
- The previous works on user association in drone networks and do not consider aerial base stations.

### System Model
- Two types of drones, high altitude drones acting as access points for low altitude user equipment drones.

<a href="https://arxiv.org/pdf/1805.06532.pdf">
![System Model](/img/posts/drones/systemmodel.jpg)
</a>

## Transport Theory
<a href="https://arxiv.org/pdf/1805.06532.pdf">
![System process](/img/posts/drones/model.jpg)
</a>
- The authours have applied optimal transport theory because they have found that they can reduce the latency of drone-UEs using classical SINR-based association.


## My Contribution
### [Facility Location Problem Approach for Distributed Drones](https://www.mdpi.com/2073-8994/11/1/118/pdf)
![Class of drones](/img/posts/drones/droneportplacement.png)

The above figure illustrates my idea of placing drone ports in areas where there is a high demand. The larger coloured drone ports indicate the optimal drone ports selected. The smaller greyed-out drone ports show the non-optimal drone ports in the set of potential drone port locations.

### [Facility Location Problem](https://en.wikipedia.org/wiki/Facility_location_problem)

By applying the facility location problem, we can devise an optimisation problem to find the most optimal placement of drone ports in respect to our budget and set of potential drone port locations.

## References
[[1] Mozaffari, Mohammad, et al. "A tutorial on UAVs for wireless networks: Applications, challenges, and open problems." arXiv preprint arXiv:1803.00680 (2018).](https://arxiv.org/pdf/1803.00680.pdf)

[[2] Mozaffari, Mohammad, et al. "Beyond 5G with UAVs: Foundations of a 3D wireless cellular network." IEEE Transactions on Wireless Communications 18.1 (2019): 357-372.](https://arxiv.org/pdf/1805.06532.pdf)

[[3] Lynskey, Jared, et al. "Facility Location Problem Approach for Distributed Drones." Symmetry 11.1 (2019): 118.](https://www.mdpi.com/2073-8994/11/1/118/pdf)




