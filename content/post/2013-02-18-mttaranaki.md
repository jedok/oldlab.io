---
title: Conquering Mt Taranaki
date: 2013-02-18
tags: ["life", "newzealand", "hawera", "taranaki", "nature"]
bigimg: [{src: "../../img/posts/mttaranaki/crater.jpg", desc: "Inside Mt Taranaki's Crater!"}]
summary: My story about climbing to the top of Mt Taranaki
image: /img/posts/mttaranaki/postbg-eye.jpg
comments: true
---
Just before leaving Taranaki to study at The University of Waikato, I made the crazy decision to climb the mountain that I had lived nearby all my life!

Climbing to the summit of Mt Taranaki is no simple feat, along the same path we traveled, people before us have lost their lives. That's why it was so important that I made the trip up to the summit with my father and his experienced climber friends.

We began climbing to the top from the Car Park at approximately 3 am to see the sun rise. This meant we had approximately three hours to reach the top!

We finally arrived at the summit with a few minutes to spare! In the below album you can see the shadow that is casted from the large size the mountain, it goes far beyond the horizon!

<script src="https://cdn.jsdelivr.net/npm/publicalbum@latest/dist/pa-embed-player.min.js" async></script>
<div class="pa-embed-player" style="width:100%; height:480px; display:none;"
  data-link="https://photos.app.goo.gl/4PSQjgehjbqPmvPr9"
  data-title="10 new photos by Jared Lynskey (인재덕)">
  <img data-src="https://lh3.googleusercontent.com/SCBsh3qyvmH2RkHG_Nc6gJL1cfAwE5SLD66QtRnWSitWGJrS9EoY1Tu6UbWldqGhlJgnR_J5exTIAFLgXnSOXPBCrMuYd_Xnw9UBEEghjdOtbGnMedb04Oe6VsY7DnlNNKJfzIvzAXE=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/KDpVohnHJJ_4BgV6j74wZ8Imvr1mZakwtPVyeiaQcMkXa84Db6p3_TsKd4j0XQdSBaoEVqgRjRrQxJib7Ri4jWO1GR2yi1iba7I5PAYkrBZdrhJ6Scbxp-4Xu_xMHf7Tem7ZWuZH5oI=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/kZxurUJeI49aF5Y3mI5JF_weoaeUyeo69ir7ocnMI1_GB-_xvp2_gdchNex58_Xbp1iYTj3vxVUiq1-l5bVhzkVqs3yaPn90bOuHZyR4jgh6jfXK4Od5aVpe90RUR86eyHqSUjv7vNY=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/-bEwarKUGal8I7NZF2-B3DAGcNkpmNKaZnnxJlu-unJI7zWkMaQGPxhOd3sHJ5VIgC4_M9anUDmgWq2hYCFApSNpv3F7MOcfXyQLNV6TCXbroKrwbR-H1sb7pxVZnsvkTI5TGyWfQSE=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/uDIkRmPzO5EpZiJnuE0qRvq7-TDGW-8Q5ro016rb3R2tAWSwmyaI67L1rfINo-XYj0uBHTxogAeBRPG0pq_xlGOj6MYUeZFWRAsCgKUYxK_QJtmqTEdf_4v7qw1ZlRL4-0YMrxo7e1s=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/KO_wkR68slOsVNcda4MzRfu2sWSNqIO_Gt8_VOviJcoxUGce1xIwEkZG433Ynv8j5leX6tp7GoYW81RQEMyEJF0BL9voqmei6qdh3m_4JDZHQnH7g2FK8-khORDXnRwSqILMnZyyZ_4=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/Ce0bYNL9XZxAvuvlwyApFVl8LbzHQXoct_92VwOF-J13EHVj_xclxqtNGMFoH4VschTbOnFVcHxWa_f8Eb_rOAI43lmTMCHWQDwTWShi1gDL2cjx0vvD32-RGU7yQQZ4joRpvoG3pqg=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/0d5fHeM34kOX2lK80BQUO6svWnyaW-4Q7Yx3H9VrANx9i87UABJWgVmbrzDyBrfcxqTsXExgE9aFnofLuel00LXIBu4p5qDzFba9wRbnBD5EQrdeS4DXokd5mVy5XcFA8t2M7ULRn4Q=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/6XW8bY27YEkA8uyIfVp7GMQ2oi-k2hE3ExPRBzdG06hOWTalVzoIE7uOy94VBvIXtgS08A97MXfq-igBRtGzHEHLa6jXDnGdLMvUGDKBUkZJf1r-I5tMJ_CHcNXBHogAbGtajjIk5q4=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/220wCpfXGQLko-uEmqF-dhlEnTWOMcpo7BOuqJCyQn0WTYGmFzYJQbljgp38gS6yMuUibMTvMJbVrEgezCOtNqkkWrCzetPrDUkgeoimauxjSz3UsYY3vYwPhH8Sh1iO-MY2_kAJYm8=w1920-h1080" src="" alt="" />
</div>


{{< instagram Bsa4MDxnqTR >}}



<iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d197189.37714248494!2d174.04377998566486!3d-39.444832980972905!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x6d1575b375de93f7%3A0x500ef6143a2cac0!2shawera!3m2!1d-39.5883348!2d174.2795389!4m5!1s0x6d15b0517747d98d%3A0xb4bd6905b2ed0ef9!2smt+taranaki!3m2!1d-39.2967702!2d174.0633993!5e0!3m2!1sko!2skr!4v1547043429497" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>