---
title: My Korean Ability Certificate
date: 2018-06-28
tags: ["education", "korean", "korean","topik"]
summary: Want to find out the results?
image: /img/posts/koreanbg-eye.jpg
comments: true
---

I passed TOPIK with level four on my first attempt.

My next goal is to get TOPIK five by improving my writing! Which is going to require intensive tutoring.
