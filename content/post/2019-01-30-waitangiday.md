---
title: Waitangi Day in Korea 2019
date: 2019-01-30
bigimg: [{src: "../../img/posts/waitangi/treaty.jpg", desc: "Military Drones on display!"}]
tags: ["waitangi", "day", "event", "networking", "newzealand", "2019"]
summary: Join the Kiwi Chamber and Kiwi Alumni to celebrate the beginning of New Zealand!
image: /img/posts/waitangi/bgtreaty-eye.jpg
comments: true
---

## What is Waitangi day?
Waitangi Day is the day that the Treaty of Waitangi was signed. The Treaty is an agreement between the British Crown and Maori.

While disagreements over the terms of the treaty continue to this day, it is still considered New Zealand’s founding document.


![Flyer](/img/posts/waitangi/queen.jpg)
The Queen's visit to Waitangi on Waitangi Day.

# Waitangi Day in Korea 2019

Click the poster to RSVP!

<a href="mailto:events@kiwichamber.com">
![Flyer](/img/posts/waitangi/flyer.jpg)
</a>

<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12651.971288107987!2d126.9758739!3d37.5552329!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcc66e35fd94ed149!2sMillennium+Seoul+Hilton!5e0!3m2!1sen!2skr!4v1548827075173" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>


## Photos from the event
![Data Timeliness](/img/posts/waitangi/party.jpg)


## References

['Signing of the Treaty of Waitangi', (Ministry for Culture and Heritage), updated 18-Aug-2014](https://nzhistory.govt.nz/media/photo/the-signing-of-the-treaty)


['The Treaty in brief', (Ministry for Culture and Heritage), updated 17-May-2017](https://nzhistory.govt.nz/politics/treaty/the-treaty-in-brief)