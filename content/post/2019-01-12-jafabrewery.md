---
title: Friday Night Drinks at Jafa Brewery!
date: 2019-01-11
bigimg: [{src: "../../img/posts/jafabeer/beertray.jpg", desc: "The amazing Sampler Set!"}]
tags: ["food", "korea", "beer"]
summary: Jafa Brewery, opening on the 24th of January 2019
image: /img/posts/jafabeer/beertray-eye.jpg
comments: true
---
## Review
After meeting the Serena at Starbucks, we immediately headed over to Jafa and ordered beer. Not long after pizza cooked right next to the brewery followed, Four Cheeses and Olives! The interior is very trendy, with a large window that gives a clear view of the brewery tanks. If you're lucky, you might be able to have a tour of the brewery while you wait for your pizza!

The pizza shop has over ten options to choose from.

## Long live Jafa!
Jafa is an independent micro brewery with plans to start brewing next month, after the official opening on the 24th of January. Dirk our friend and Jafa's brew master, gave us the grand tour of the brewery. A total of ten tanks separate to ferment hops into beer!

## Gallery
<script src="https://cdn.jsdelivr.net/npm/publicalbum@latest/dist/pa-embed-player.min.js" async></script>
<div class="pa-embed-player" style="width:100%; height:480px; display:none;"
  data-link="https://photos.app.goo.gl/aChEDw1Cf9LEuLZb6"
  data-title="Jafa Brewery"
  data-description="13 new photos · Album by Jared Lynskey (인재덕)">
  <img data-src="https://lh3.googleusercontent.com/-w8xqxGmz3p0WLbBO5vNDKhUgDcPk6Y-gWY9nr_P3_vH8vVZaPBQSVH5-TfK5quX0RT_TouSnRNSFXdQIFAeaH5-81VhP46NZPLftCTbpLoNoubcYedVeSAy4-P2MrdyNCNePFMpAB4=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/QbFvaqM0BZ6AUiblDlHzy4iIWd5wkKAzl48AkLraZYcSYfBIEfStEcw9pvFet7Zz62RWNm4R6_hGyBPa_uHpDPRMCa2Gxa8RPCx_-X7HagMP8Rl1FVoHtkW-E32BCbC2uhTyURwtim8=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/v6e6BiWi74YK3lipR5-JbDQCvmzxiqkGxje_2kQtFxiX9ehCSS36pzIfyLGbWcYOOsdhH8YggdgKpyO-5coNTR5WXDPEpDSucR2tg2MGzHDmz3h96hFMvZu2KFEiRFDTpQBFNpz8fws=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/6hy_4VGM2MIcMJXQEu35yOLTp2V5t7PJuqnD8GJecUvXhLV9lxapK6ZkOxosL3T6eYd2hPfPz9P-BKG8bkuAg5C0gR2CSdppaJxPGWCUEgtqV2gpj-1v_RvGoBQGXdy1L7JHAy1XDeU=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/aWI8DD43HB94gQPYiaSOR0FXyDwmK51rj_keK_87K3ZmQ7vAWcYyJAArxpz5IgmGFWEo8I4RZl0C0u7-N7I_8G9DV4TxTwKhNeatg2PPXLdy2wGnZT5smG_7I3B6Mgbgoa-Wm4SsWAQ=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/Z6mpNkulbohs9t8qTpyntkwFp3eE3Oj-m7LAJodTwB-UGbar0rG_t06LUzf3BVMbuyZPdBSQJCSRQN7tt5ZS6lp-WgyAWPdAmur4zM3LT7Ww-dxy4R2loxcYUjXcRyrjUcT_O67dDHA=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/_J-uY17luJb0Qj0SHvKxmaSsGyqN0E3i6qBH-_fGBdb5PPbmE4AGbXtLUDBxK8nuREFLhzP4NIvYZmtzSbf7uIMXs3jbpoOHD5mAXfcpI4q5HS2wUlyPF87M0m6JDg-m-BgSBj0Pvvk=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/HNkBASKj116E0LRNMVB7xce38CO4TlmnzogElhJxyH68K4Ai2bPH9DoNF74DETSQKNddTsRtZdvn2owFxv9aJk0JVeXpiHAuRpeaJVebHhJGcmbqKyN-YrqoLBYNStz2OnqukYxl3q0=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/WP64OlMCcbJl98I7ca0Lfugsb3goBu2-by9i0S5N9uM48_1A42J2cDb9sNldqHYYAH0YQCYELjeIPWehZMMEf5saY1-ja7VgrG_rMCADQc9FnNC3x1Ti3oBTFc9oQxCDmaVBvFZ83to=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/kHZn1D75zV8V535DyvMQAtVH-xnHuDkNOrjPx72p29saT2dyk_yf3m8RnyerBHT2T2TAcRqkKynYOikgFbkKFbljOKfgxmEX0Y0agJAjbBYPxg2D9nYf5JmAHZZ8RoOvWvTv3zIRjHE=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/LAWx3mcyh4W3GfKvOsrp9PDpgWqeuj8iyH7kYS5p7Jbf8X41V1fvIYUNg5a7D9BWni99u4ylpm8DRzrJ4X5Pr5JW5kkugJjOFLYGC-lgnic2RhM7rs9dBRKQA0piYDMuOWuzvft39Hw=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/y13Io9mX5eDIntl6VwY4Yv8LVSZ7io9N2cyDu-JhMHSusuI8AAAkaCXpUR3u2nPsteMeytzsCDjsTfc2376QbISCaaRIkaYh4SrDLrEqVMh3WizeTDdxnurLtjHc_j6oz4q1rh4Acg0=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/Di5t4ksAkygRJA19okRKMLbVQZo7xjZvuD0M7toxn-bdA_LE9ALNYADVMuc07beRJvApwOQKpMjrShFCJtlOGYnDzXgFeJhC-_xrac80KotZhUckYKbvHEhi-9WJ9PeBD1ZOk7j3RC4=w1920-h1080" src="" alt="" />
</div>


## Menu
Their current menu contains some of the best local beers around Seoul. I recommend trying the Sampler set as it the most economical way to try a selection of beers!

![Menu](/img/posts/jafabeer/menu.jpg)

<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12654.306294527456!2d127.0569489!3d37.5414775!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x95148ab340a60c28!2sJafa+Brewery!5e0!3m2!1sko!2skr!4v1547284506615" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>