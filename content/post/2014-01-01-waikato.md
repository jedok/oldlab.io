---
title: Waikato University
date: 2014-01-01
tags: ["education", "newzealand", "life", "waikato"]
image: /img/posts/waikato/graduation-eye.jpg
summary: My Story about studying at the University of Waikato
comments: true
---
![Waikato Logo](https://upload.wikimedia.org/wikipedia/en/b/bd/University_of_Waikato_logo.svg)

After completing my studies at Hawera High School, the time to choose a career path had come. The first choice was to become an apprentice electrician and continue to live in Taranaki. The second, was to go to University to study Computer Science, a long hobby of mine was experimenting with computer features as a young boy.

The thing that attracted me the most to Waikato University was that there was already a foundation laid, my brother had moved there, and I also had cousins living there too. 

For the first year, I stayed in the Hostel and met some great people, including Serena Lee who I will introduce in a later post!

I studied there for three years and I only had to change my degree once!

![Parents](/img/posts/waikato/parentsgrad.jpg)

