---
title: Age of Information in Wireless Networks
date: 2019-01-23
bigimg: [{src: "../../img/posts/aoi/ageofinfo.jpg", desc: "Living in the age of data!"}]
tags: ["age", "korea", "data", "wireless", "5G"]
summary: Coming to the next generation of mobile network policies.
image: /img/posts/aoi/ageofinfo-eye.jpg
comments: true
---
##  How old is your data? 
 Data Timeliness is one of the six criteria's I identified while working at Methanex NZ, new information has much greater value! Recently, researchers are now proposing algorithms that schedule data collection from sensor and IoT devices to minimise to the Age of Information (AoI) in next-gen wireless networks (5G). 

## Data Quality
I conducted research on Data Quality during my time at Methanex NZ and these were the key points I identified when evaluating the Quality of Data.

- Data Timeliness, The data’s value at the time it is used
- Completeness, The measure of data in the system without missing fields.
- Consistency, The measure of which data is kept consistent.
- Confidence, The confidence in the source of data.
- Accuracy, How well the data represents reality.
- Integrity, The number of sources and level of data consistency.

### Welcome to the generation of data.

![Data Timeliness](/img/posts/aoi/timeliness.jpg)

## Age of Information 
Age of Information is concerned with the freshness of data [[1]](https://arxiv.org/pdf/1811.06776.pdf). The recent wave in Age of Information is due to the transition from 4G to 5G. With 5G, there is more focus put on wireless technologies providing service to Internet of Things (IoT) devices and Machine Type Communication (MTC) [[2]](http://vbn.aau.dk/files/208323943/Five_disruptive_technology_directions_for_5G.pdf).

## Latest Research on AoI
**[Reinforcement Learning Based Scheduling Algorithm for Optimizing Age of Information in Ultra Reliable Low Latency Networks [1]](https://arxiv.org/pdf/1811.06776.pdf)**

In this paper the authors consider a factory environment where a number of sensors are constantly transmitting data based on their current state. These sensors do not contain any on-device storage. So to record the present state of the sensor, their signal must be selected by the remote scheduler. Other sensor's signal will be ignored by the by the scheduler's access point. Hence, creating a dilemma for the scheduler to optimally select heterogeneous sensors so that the time sensitive requirements are not violated. "The [URLLC](https://arxiv.org/pdf/1811.03981.pdf) term minimization is represented by ensuring that the probability the AoI of each sensor exceeds a predefined threshold should be at its minimum." The authors propose a machine learning based approach to solve their proposed formulation. 

![Data Timeliness](/img/posts/aoi/fig2.jpg)

**[Minimizing the Age of Information from Sensors with Correlated Observations [3]](https://arxiv.org/pdf/1811.06453.pdf)**

In this paper the authors study "the average AoI in a generic setting with correlated sensor information." We can minimise the amount of redundant sent to the access point originating from the same source. This requires an intelligent policy to schedule the access point to receive data from sensors with varying transmission rates and data sizes. The Machine learning based policy allows the scheduler to identify the sensor’s source data and receive the least amount of data to minimise the overall age of information. 


<a href="https://arxiv.org/pdf/1811.06453.pdf">
![Data Timeliness](/img/posts/aoi/fig1.jpg)
</a>

## References
[ [1] Elgabli, A., Khan, H., Krouka, M., & Bennis, M. (2018). Reinforcement learning based scheduling algorithm for optimizing age of information in ultra reliable low latency networks. arXiv preprint arXiv:1811.06776. ](https://arxiv.org/pdf/1811.06776.pdf)

[ [2] Boccardi, Federico, et al. "Five disruptive technology directions for 5G." IEEE Communications Magazine 52.2 (2014): 74-80.](http://vbn.aau.dk/files/208323943/Five_disruptive_technology_directions_for_5G.pdf)

[ [3] Kalør, Anders E., and Petar Popovski. "Minimizing the Age of Information from Sensors with Correlated Observations." arXiv preprint arXiv:1811.06453 (2018).](https://arxiv.org/pdf/1811.06453.pdf)


