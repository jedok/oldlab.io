---
title: Hamilton Gardens
date: 2016-10-15
tags: ["Hamilton", "gardens", "travel"]
bigimg: [{src: "../../img/posts/hamiltongardens/3.jpg", desc: "Italian Garden"}]
summary: When in Hamilton, do as the Hamiltonians do! That means visiting Hamilton Gardens.
image: /img/posts/hamiltongardens/koru-eye.jpg

comments: true
---
## Things to do in Hamilton
When in Hamilton, do as the Hamiltonians do! That means visiting Hamilton Gardens.

{{< gallery caption-effect="fade" >}}
  {{< figure  link="/img/posts/hamiltongardens/beautiful.jpg" caption="The water of life!"  >}}
  {{< figure  link="/img/posts/hamiltongardens/china.jpg" caption="Welcome to the Chinese Garden!"  >}}
  {{< figure  link="/img/posts/hamiltongardens/5.jpg" caption="Enjoying the Italian View"  >}}
{{< /gallery >}}


## Tour de Garden!



{{< gallery caption-effect="fade" >}}
  {{< figure  link="/img/posts/hamiltongardens/japanesegarden2.jpg" caption="Enter another time!"  >}}
  {{< figure  link="/img/posts/hamiltongardens/japanesepano.jpg" caption="Japanese Garden Pond"  >}}
  {{< figure  link="/img/posts/hamiltongardens/enter.jpg" caption="Enter the Hamilton Gardens!"  >}}
{{< /gallery >}}




<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12609.44978158966!2d175.3052025!3d-37.8049786!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf00ef62249d2b80!2z7ZW067CA7YS0IOqwgOuToOyKpA!5e0!3m2!1sko!2skr!4v1547365135296" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>