---
title: Korea Software Congress 2018, PyeongChang
date: 2018-12-20
tags: ["education", "korean","computer","science"]
image: /img/posts/pyeonchang/pyeongbg-eye.jpg
summary: This year Korea Software Congress was held in PyeongChang, check out the photos here!
comments: true
---
Every semester our lab attends the KSC and KCC domestic computing conference.

Official KSC 2018 website [here](https://www.kiise.or.kr/conference/KSC/2018/)

<script src="https://cdn.jsdelivr.net/npm/publicalbum@latest/dist/pa-embed-player.min.js" async></script>
<div class="pa-embed-player" style="width:100%; height:480px; display:none;"
  data-link="https://photos.app.goo.gl/sKdNb87zMhqxB3Ae7"
  data-title="PyeongChang 2018"
  data-description="7 new photos · Album by Jared Lynskey (인재덕)">
  <img data-src="https://lh3.googleusercontent.com/iPRx9kuKyqmb3Ind37ii9ah-1g1VYOtNRS6ZiflFo-WliEmB4-iDKeW50Y_EO1OOGbi-uSYvIn8FSMymxam0kXajs83UnhLPbbNaR89RH2iu1vi-zXYdbI7Xj2FbxLcvH8Ps4jFtoa4=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/TO_k_CIUjicRObGzGIvS09ZNZjzpW5ALJShjS6Z4PgdsrruiderdT711TDKK1lBhA4kOPreQ4rlnV7K94NfgXudv9HSIPBv4-4Q2mP4nXCWI3xzwWqXchNBjLBMGNnc4dHLPRlzAmjY=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/PGn-Z6eabV4JJb7QAupRZzAX8-XFaBXhumaLnI6XdHOFl2VmwOqXCTwDnpDUD8M4Gz0qXNwYEwwRz-Pls9O0js4PI2RBPXJj4aD9feOs8xzMxIIbOJOlgLwyqv0wuWREDrC55Peze_M=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/r6RNiusYZaoTdOVNQJID9SULXLCAWlj570SuJGwiJb4xoFRS-JBvl4wLk_WwowtG581ZH5D9-j2OIi0-y6f43_m06EHouHb822XMED4Cn4KxyR_2PUQIcCq1X1JYj9O99B2hiyd8jvg=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/13VA6bRfjmibAAz-9ZE1njbefJufI4o47Z18x40ncjKjqshO85EBxpV09ntPrGsRbw5DvY4MTrNQLKAvK1dPZcG_MshokoeqGnjf8T1Ue7wa7FxYIgPnfNyKvX6RshjAz3XkFFdUWoM=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/tmL9YAMXXnPMElcOm6rO7DfTgvTv6ZgBJBPPaEDczQzQr7AL7DGr1YUcS8yX-I5f18wag-JqIAJNGQCLccqry6BFDyjQ8foirMRciq_xi3cB4XL7R7euJ14dnUwsdz4F39BLI9SzFv0=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/0F4NaBXGChZ0YFiXnFhruMBCOo4g2uv-cpskFiNR8Iq0SCdM1KvvocgNHwSng97Sm0u7yYGv9w22MaXBpJ5Xd7Fsh-ZZcukkGtiE40p6mUHaL0Kh2dGpUH648QSfnrtPx1B2B0wPl0w=w1920-h1080" src="" alt="" />
</div>
