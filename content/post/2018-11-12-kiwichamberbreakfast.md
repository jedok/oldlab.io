---
title: Kiwi Chamber NZ Leadership Speaker Series
date: 2018-11-12
summary: Taking photos for the Kiwi Chamber early morning!
tags: ["education", "korean", "photography","trade"]
image: /img/posts/kiwicham/breakfastbg-eye.jpg
comments: true
---

Kiwi Chamber has hosted multiply events breakfast this year, the final one was today. The guest speakers invited were [Dr Pau Medrano-Gracia](https://unidirectory.auckland.ac.nz/profile/p-medrano) from Auckland University and Professor Kim Gun-ha from Ewha University.

## Dementia Care Robots
Professor Kim of Neurology gave a lecture on "Dementia Care Robots" Including the daily life support function of a caring robot to take care of dementia patients at home. She explained about robots that can train continuously to enhance recognition capabilities and cognitive training programs that utilize them. She is also co-developing robots that can take care of dementia patients in homes with support from the Ministry of Trade, Industry and Energy. Robots developed in this project will be evaluated for effectiveness in 2019 and will be operated after finishing the task, leading to the development of dementia-care robots for households.

![Dementia Care Robots](/img/posts/kiwicham/robots.jpg)

## Uniservices 
Dr Medrano-Gracia introduced his Role within Uniservices as the Business Development Manager. They have partnered with the best minds at the University of Auckland to apply intelligent thinking to ideas that have the potential to change the world. 


## Upstarters
### New Ideas From A New Place
If necessity is the mother of invention, it’s no surprise that world-changing ideas are born in New Zealand. Introducing the New Zealand Tech & Innovation Story.

![Drones](/img/posts/kiwicham/dottereal.jpg)



## Photos taken at the event

<script src="https://cdn.jsdelivr.net/npm/publicalbum@latest/dist/pa-embed-player.min.js" async></script>
<div class="pa-embed-player" style="width:100%; height:480px; display:none;"
  data-link="https://photos.app.goo.gl/ZNVotgWiE8N75wpe9"
  data-title="Kiwi Chamber New Zealand Leadership Speaker Series"
  data-description="41 new photos · Album by Jared Lynskey (인재덕)">
  <img data-src="https://lh3.googleusercontent.com/S_fiPGQ-7UMgrKajgQea8ArfZaVFKheFriodbSol_G-pStF7yAXvBueXxKXd3WLz3xS44fCB6iAPw-3fx6ek9ibPOcgcHjFROsH2C821GpAdfY-k-ewtgxxFvMG1faI9ZAOd0D0pDKY=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/Yc4aEIQLfyirjLMUg-XENgYh8rnd9vRgm71k7tFsUYvSgd9ANr0O1lh6BnTyjexAVTmg1h2EUk-Wvy8P9oDVVT5oO9BYG2Z8_M4btbE46y2TrAGvV8RJ6U4FTopVu0bEnl9RIHcTu8M=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/908QZ8mnnMGPhxlqeKLl2fT3vjgOvDAhq8oRhRCKO89yYf4A8dlF3ssUri6iVf77MphwVsDTt8hVMS0kWG7TQIcGPWz2i4_NBdUT_ycawJd77j1ikQXdPO3btW9QpRLstEjD5KAqLow=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/cKnfbhH_O1I3Sjbh7Rsj6Sisl4sNFyh0QH2jXe_1t2eq8lykfwrkpKDCPwDVBPPGFzhIutuFZmjc0kn-2Vj9xn05mhTn70-cTEm1Xgp9bZCyWbSwIC2UJ7VUTKxzveXRyw-csByJ12s=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/2-ZsIjK7X5lPdZ9Ndek2DVkZKWmlDaTuhA2KKfomKcdE0D7SioIXEJJ4GTYxTYszhgipVtSm2YH6CWs1zgpAqI2wkrFFA3yLaml5YZNPIihoYDatfil-vUkFoK2DkzbFGRDasvztzL0=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/wMk8BC4YOLYu5ifL7ZzGWXxJcR3GBDq9vlTrqrEJ41iO4UB9sVchml5QL_VdxUWpEMmLZqIzzJtAETlAJ4Y5_6cK_5Rjl1cZYRbsZXtyV41Qm6S1omAtSSaO4f1sKNz4vP1ytI54OmM=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/Ny0D6YWSNFFMgikFlWNqNzzxFSKKdS95-aTEUU-CTn5vNbQh9HJ-w-zFov1scelUSaLNch3Un3heDuR8etKXHeJ4tB-5BSJUIwhiMU8nAxIsdZBu_diG9HL5hHCjTEJuwN7LQ8Q5rwI=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/ZSwhUdLytH8FS4q2msA6MqZD60bQ1Y4FKkKjaGc4QqN2_-UmsJFAFg9z3Zx0Jqd38rshACIoUz9u6VHM1GJEgdf4zK9L6j02UDom4xHls2L4XrPqLjJRF6aOK2A-MezYF360_Yzh-pI=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/F0LuVNojrNEwEqQaZbn4EsJNs_AWh9cYpKNMikjSMSCzTdOkNHqpuoBgzfCr017cj9JG-9KAe7m7VlH3QtyJXa9eHnUzsKZIwjNaKzl-l4M9j23EGEDCkSssoe_Rp1fiaZ4w_w2JFho=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/prkpXPUphIJPFOn9QWHjmkdko6NF5T3IIUXIyJmpwyJZR-DA0LDA-MySRfgPsTF8NmG-IYQP9W2IFBoPq3MGC1ifEZOfWnkGcVvSSKb5tUmaHkI6_EjFyu_Nw3Gkw9m0DYaZtUircQA=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/1YWZnRhxCJmBrG6XzoAkEnCYrlYFRwA4ov5uZDQNZ9wqZskGQoYc3QD-K0UZbnvvbOQpI7vdhcTrLhvLiYCfIKbB_ZTKXUbPgfTgAPG8ufOjoULSgnVGi_u0L17zlFvgKcRGzSRcgZg=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/T11eYvaBHWxnUkpT0NNdnb1rxLgfJ6_tlYrUmV7G3_PZjIb3qtBeFFAVeIMLl1WvjYrM3NBqS2Zyd-xQwUHQAzehYOdKo_kDjNSMqi2gC_FIok1sBpvj96NUOM_KVqVMRuFhFN6u97k=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/YHvb6sooEQpQVcihuacXRav2-1huhH1dK8YmIKTp_6Ab5SDOO7ll3QL-fJIb6PGd2iJNFpqEmjKvSk8SnzGUMTcBtmN4f1eWwe-q0F-yAz4hCr-2dVBtfFxLHtlFOETCnUzbkDc0Wlg=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/6T1fv2Q1Gu8Ddp_WqNEeaI3GAi9X-LkmWZzNtiy7sYu9TdLtsJGex7XD64XMU9msqGx7b_6RWsliFQ117CPwjY2OcFw9rF9zVDg9gPVZ49QuAFiBEJe6-bkLjOkWzSXx5nD9o34c1GI=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/kTh1W99O39xuC-mYNdTNKpecNKFGeHDNUW4ubPXXtBM0wiXPCwiDc5eFALhOyWt04E1Pn6wnuRyfu71oU9wUQ0-Sba_nvFDHuvhXJXpMwo5M-5kyeAZrpceQtL_2SeqmUGqTNkMmE6Y=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/MC9mw2-C69GVeA_AQDSXnByliQc9ylxWMSYbGwGe86yXP2umBpkNI2dLjXCzk70eS_x3QSMVrJ8Q9lhhTmjCrWGtCc7wmTG3Xr5TifebGUW93vbU08tHJGhl6jETdVg7md4BuCDwUSg=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/o_05UWzm5GoPsd4IHF8ekUa1RAUXqvtdDe8_7HADmR9LmzNmaaOF4HYATqOo-sfi8mLjv55UIJ4Jez8GPYw6p7HTs3V5sme3hEmphzUdBeENj_KVSqsS2ZzuqqCwIrK_DZPzU592RzM=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/Ey36lRjVow8B2l5rI6jqsIZeSm8ptn1YvDB_tS30cgmIlwAHL1AobKZeNT-FAGPGxd5GNWgtj25RzmZ-LFVvCHWBYJQ69QciUN7qCvrAi9pYjmco973qK02PMj_jfMotHlAEzcF0D0c=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/KEF15Rb_Xi_OYdWZZXF968cUtmXqT8cN_6zOjaVpANo80sL9r91pYbmdnF8jvuaw5wjl9ErGT72k50Ik9tD8vBq9wI0aTW9anTquvqF-AaMxMjufHRMsXLy2mza0KAwwyToMSgt4K2g=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/5qim1FL61m9x8v0lYabg_9R-v-SVeO36JOY_VpquisBR1OOKiGLDUGJG2mcU6tdrYFc0n-FrA7dkh_P5g3oo8JQ-Fu-7q_CQil8YkLMcvRPzRrFTJs2okLTqFSog59v3jTlN7wWPeAU=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/jdb6HudyJqin-RIZygtWo9ytPVX3vTsC5mT0RoQrRG0SsUsmmcz8y-2ghqMN5uhIVHnnFtbjjfUuYoTUPv-WmorBNevPJ2UJL-ywkbgCKEhmoaduFaYwFcBCthT-qlWlAs7NHaTMND0=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/3ztK0Qb-l_sWlmaxCMlzw5rIHlHrQ3vtvjkt7etBM9p2m-afmWFQuQddMF9PUQZiWUvr_Q4w3U3VG7EY3sdKPPVO9r8NIpJoPLJTdBqN2FhyREmjNL-Whcw5QLjX5sPaTFEXA4sAHSk=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/T9OBo-lAzgmFS_-Z1asZTYz7JSR4CD5VvI9xFgl8pjp3Js-U1FJ0bogvO0Isr0BvpS4L4N2Aq3Uqqm0Iz7fFH0ke4ekR2Y2VGPabKVrPwfLIiZaRVX5nVQzxAWWKNXuuFcvNQFXE6ik=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/Jwf_HRGx2dQJg-q9k4U5soM5vkwT-AO0zm9GCk8sTWZwisWFwkCLp92N-_wWBa3dterU3aodSsc8TuZPb6zqounFKbwSsDdfz6MfemEZf92c5RCdMrFCquiB7uohYs2t3l5eAtVmXnY=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/xEbSmy6mEJG52RWl77bGxusYIzI5zqel320tOShq8Kwqi1YK_GZis2Qhi1G6Xp125Naxb6DsbOXaLMkP2QhmeG4HYJnwEXxrb9ssRsJZJfi5wsrtSCIRNo-xUyqRPec7z2xWXzQWIvA=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/sSoiWMV6jl1rGWr2IHssqFeAeg8AKCtErRTaZX4XItaE-dbHAEZ_nJI2LzLdDAA_JKO7l3Edn609CDm5aVq66jnwXcv0OdJwIm1OAhXPo7qZKzZp_ENPJTVzVzWf2mXku1Fzn7YmnhI=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/xHKG_sGa3i3zkugAg7R9fCJMUzjsKoqQMwJwpTb6eh-6H8jndaqE-7mMSA0SSfqqle66O6uaHOOh921CutBSr3KCrXTdMZrJfCnN8FSo0rK0UJPhXECCu9KQA5nJqLh_jSqDwEK6_lA=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/EejNdzv77EQhUfADPvU2I9agBQlAH8lzSX6k4rWCQg1-OzTkN4PfzpRzMuShuADCsXzdAnvQH9Apjit1-RvxLJQ-AoFoSfx7F7eqzVyj8EiKeMOJ_kAgoy8nuGYQd9Sy7VfSuk9i_10=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/n69hLebjD_Vns6go_OnHNE-3WKXEuSmE7dg3VJZNC7ofjB8xfG0cfG4mqev43hmgpJucTtxpDslMTftaz3p0M5TXv_DH-4CO7cGaFxvPreHZFnnx2R_PnBfHhpqBJiTzxOi6VPlSYks=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/POD3ZSkdrh_csw-7s7zMxQRWBrPIODC4uuQSCrwrMXaS1nnDNIayPV-TB4PY4aeOnlp4Y1x5TjsngNUI7bhJCzRRRuVnt4moxHhnx2UYRAoM-OT3L-aPvIVOqcFH9Uz-t7pPHt_hmjo=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/oW_evbWI56KNRYHXUIFUFVxvkZIyP4KqqeIaiuzhMfEBDNPDREq5f4_vw8X6gQcHlF6bRCIFm2k148d0cNKm1krhLgHL0wVd8Qifmz6eI34asQGs8_EDSigIeiAWlJfX0jaJvggSh2Y=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/QXgAPlwJNM-RYtxQfXZDk9gCVrjWdeVElshVgD_sveV860MuS8uPIzTuQhsnyRPYvQolA3IPCIgv5f7rmaMLm9FyROZuGbQ-5_V0ANHrtpEkWVXGIvpZ44FtPbN2Fg-4F0MdUPbKev0=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/b-kqe-yt4bPCfJGX153QugpdLPsu8QjOvsK11rXqTIUmE1U9voGzw74pegKWxQAHMnOiHi7PuletGR1kARE0vfX8TqNuco2-l41ct3gJZWOg6CHgZyihv-szyV0m7S3eyMK5UAvX0Nc=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/OxygD01grt445EXyMWYS0mmxR3ioaT14FdDTsO7IjdwaFUZpXiEhIr-rSuQzooKPGKdrZfWp_wwLTvh8yEJH3_xqvTcYXGEJkqLHvNk35Y_TJfdD295WZ19Pg6T5_xKR132S2gHnBwE=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/fMnXj8X2nbgu4gcMPbpV_we6rmnr6dtTsQhUV26fTz9iSoQ1G6Nf3q0HXS3jtk9Ge7mFduc0hHM3U5tKdYwxcMwtkYv2mnv1jzILx2hbC5hue_xC3eWCe-0cznAvoaNhR5LjUaG36vc=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/te_ljSUiJYg3HqrsxjLzktMf09IXRvJJj_JB73A-k8U-z4UaWTB3qqxHlz2WY9YMExTap_vqURPoJXtiImcbgQPHDfxF1Hh4MC_-uH_uT35GevmjbnT5iwD6h5UmLuCikDjbVaDbHxU=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/-w_ozI7CeSRJVMqOVO6whjbckCKr9CXO2sNfhFYasBFbm5vQoR7XMH2Ob7N7mfCjWvoAlyOLF2dv4-_HEwRoWzytVNsooytn1a-Nz4mQnZrLOQUQ3U4YWd8PX0Ts_075f6N4TXpGaZw=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/3z8gqjCv2j21MOTlByi61s-ACd5ScdawtJvcNnmtF3yFffZeKrv_IEm-7CeBl4ptUTgjki8eDbDmToBsMAAyo2eAz-wWiiKA4GvSe4ZssoFN6ocPq3wuN5cbV-64EzEbbiczQs9yJrg=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/_Ho6_iOaZnY33zej0CeUEG4CA7JEdb0kpJZ5Y3l5tu22EeOnWGwVZfYdFN1xZ8hREPQ4-lbvGvF-8sZjZW7JlVfEwCA1uVlDwGqfuDRY6ElsqNXq6ZK2-GgbO3_yEkK_JrTq8O09bQk=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/FdiBHRtCOLosHzSBb_1s_kG65ec8mXb8C01p396FW-31UydYw3Af-Lyf3aAkiA6hia94xjmhyq-Kp0qWCo-nYvY3fSSPpUHNGOMtDKfmesYAIVfKC7Y_rd99Tq6MEYeWYfOCcW186uA=w1920-h1080" src="" alt="" />
  <img data-src="https://lh3.googleusercontent.com/Bh1qAqPJNxiTyv44Vz5F2kTpIdJ2tdMLqWx00jGXZ38PoVHCQQA1ah2ovTwwJEQyZNNjK2eNrcKgVFTiQRqD5Sx1oa-5ZSn2w-eWtibnceks39V0IC4znPuRpB2tzH9dGg7ddSWdXGA=w1920-h1080" src="" alt="" />
</div>

I really enjoy volunteering for events with the goal to promote the relationship between the two countries.

For dark rooms, it's important to bring a fast lens! Imagine how distracting it is for the audience and speaker if you are trying to compensate the light by using a large flash of light! It simply isn't a good look!