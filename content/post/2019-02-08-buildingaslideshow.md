---
title: Creating an API key free Instagram Hashtag Photo Slideshow
date: 2019-02-08
bigimg: [{src: "../../img/posts/slideshow/bg.jpg", desc: "Loving the Instagram Colours"}]
tags: ["insta", "instagram", "events", "slideshow", "newzealand", "2019"]
summary: Join me on my journey to create an API free Instagram Hashtag Display
image: /img/posts/slideshow/bg-bg.jpg
comments: true
---
# Foreword 
I sincerely hope everyone had a great Lunar year holiday, I certainly did visiting the [Korean Folk Village](http://www.koreanfolk.co.kr/multi/english/) in Yongin City. Around this time of year, not only is it a special time in South Korea, but it is around the time Kiwi’s celebrate Waitangi Day!

![Korean Folk Village](/img/posts/slideshow/koreanfolk.jpg)

# Introduction
The Kiwi Chamber in Korea organises a variety of events on New Zealand public holidays. The next upcoming holiday to be celebrated is Waitangi Day. Like every organisation, marketing and gaining organic exposure is important in maintaining a reputable image that keeps people coming back. The problem is that a lot of organisations and groups constantly lack in retaining guests, often with high turnover rates.

## Measuring an Event's success
Events are inherently social — both in real life and online.  Marketing plays a major role in the overall succuss of the event. Marketing in terms of event should not just focus on the the advertisements, planning before the event, it should also consider the marketing during and after the event. For example the engagement and satisfaction could be some Key Preformance Indicators (KPI)

### KPI's that are important are:

- Attendee Event Engagement
- Social Media Engagement -Using an event hashtag can help you gather a wealth of data on social media about what attendees are said about your event. Beyond hashtags, social media can also be used to measure attendee engagement with sponsors, especially important if you’ve offered it as an activation.
- Cost per customer acquisition (CPA)
- New vs. Returning attendees - This KPI is crucial to making sure you have a healthy flow of both new and returning attendees. People can lose interest in events over time — some attrition is a natural part of any successful business. But if almost all your attendees are new, you know you have a problem.

## Identified Problem
The problem that I have indentified with the groups I work with and the event I have been to myself is that a large number of attendees are visiting for the first time. And after a lack of engagement and disfactation they do not return to any further events. Some places are surviving on the fact there is a constant in-flow of new visitors. Was it because there was little to no engagement between the organisers and the attendees that causes people to be dissatisfied? Or was it because the attendees didn't have a clear idea of what the event was about or what the goal of the event was?

Therefore it was clear that there was some issue in engagement, and misunderstanding of the event's purpose. Secondly there was no effort to market the orgranisation's efforts during the event. Which means missing out on a lot of potenial organic exopusure. Thirldly under utilization of the equipment at the event, the venue for the event typically has a projector available, used for displaying subtitles while a guest speaker is speaking (한국어 and English). Then for the remaining duration of the event, the project is no longer in use. More engagement with the crowd through smart tools could be used to engage the 


## Solution
Thus, the solution to encourage engagement between the organisors and attendees was to create a display that gathered and displayed the attendees images on the projector to show what they were doing at the event to other attendees, and also their friends on Instagram! 

By using Instagram, those users who are activily sharing their daily life already have the tools on their phone to engage with the organisors, provide feed back and furthermore show their extended friends what the Kiwi Chamber event's goals are.

Thus it become my goal to create a small web-app designed to take images from Instagram with #waitangikorea

![Display for the hashtag](/img/posts/slideshow/interface.jpg)



## Technical details

### Front End
##### Tools used:
- [Jquery](https://jquery.com/)
- HTML
- CSS3

Since this is only a single page, I chose JQuery as the backend to control the content of the dom on the page.

To maintain the state of images on screen with JQuery, I used an array that is the same size as the number of photo spaces. Everytime an image is displayed on screen, the url of the image is added to the index of the array. When we want to add another photo, we must ensure that the image does not exist in the array. Below is an example of my code.
```
var img_on_screen = new Array(number_of_photo_squares);
var post_index = 0;

javascript.js
while (img_on_screen.includes(post_data['img'])) {
        if( post_index > post_count - 1 ) {
            post_index = 0;
        }
        post_data = posts[post_index];
        post_index = post_index + 1;
      }
    img_on_screen[curr_index-1] = post_data['img'];
    update_img(curr_index, post_data);
    }
```

## Back End
#### Tools used:
- [InstaLooter](https://github.com/althonos/InstaLooter)
- [Flask](http://flask.pocoo.org/)
- [Glob](https://docs.python.org/3.7/library/glob.html)


### InstaLooter
Instalooter is an application that allows users to scrape images from Instagram without a username or password. This is great if you require images from Instagram without any risk of your own private information leaking on to the internet.

The main reason for using this application was because it does not require you to enter a username or passwork unlike other scrapers available on the internet.

scraper.py
```
print(insta_api)
if insta_api == 1:
    subprocess.call("python -m instalooter hashtag " + hashtag +" "+ path_to_json +" -d",shell=True)
```



#### Flask
The intial reason for using Flask was due to it's simple set-up I used flask only to serve static files, however I was have cache problems. This meant everytime the client requested an updated json file, the returned file was exactly the same as the file served previously. Causing the photos to be out of sync on the user side. To overcome this, I  changed from directly requesting static files from the client to creating a route on flask that gets the hashtag and img file name. Through this, I was then able to set the cache_timeout=-1 to prevent caching of files.


#file structure
```
@app.route('/update', methods= ['GET'])
def get_file():
    # cache_timeout is in seconds from the send_file options
    return send_from_directory("data/img", "data.json", cache_timeout=-1)
```



##### Glob
Glob is a great tool for selecting files that match a rule. For example if need to combine multiple json files. I first need to load every json file before I can merge the content together.

scraper.py
```
for f in glob.glob(path_to_json+"*.json"):
    with open(f, "rb") as infile:
        result.append(json.load(infile))
```

![At the event!](/img/posts/slideshow/party.jpg)

### Thanks for getting this far! 

If you have any further questions, please feel free to email me!