---
title: One Saturday Afternoon in Suwon City!
date: 2018-11-17
tags: ["suwon", "castle", "travel", "korea"]
bigimg: [{src: "../../img/posts/suwoncity/evening.jpg", desc: "Italian Garden"}]
summary: Lovely afternoon with Minh in Suwon City
image: /img/posts/suwoncity/evening-eye.jpg
comments: true
---
This would have my favorite neighborhood in Suwon!

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6349.544290797213!2d127.0106609481635!3d37.276829919016976!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x357b433712e3b359%3A0xed5f56586bcb4331!2z6rK96riw64-EIOyImOybkOyLnCDtjJTri6zqtawg7ZaJ6raB66Gc!5e0!3m2!1sko!2skr!4v1547369071345" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>