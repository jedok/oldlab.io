---
title: Move to Korea
date: 2017-03-01
bigimg: [{src: "../../img/seoulpalace.jpg", desc: "Seoul's largest Palace"}]
summary: My story about moving to South Korea
tags: ["travel", "korea", "life"]
image: /img/seoulpalace-eye.jpg
comments: true
---

It was certainly inevitable Jared would move to Korea after developing a strong interest in the Korean culture and language.

{{< instagram BQ45bc8B77s hidecaption  >}}
