---
id: about
title: About me
subtitle: A little more about me
comments: false
---

## Personal Statement

As a global citizen, my objective is to break the cultural barriers between international and local technology companies to foster forward thinking innovation. I have a strong interest in both the Korean Language and Computer Science. In respect to Computer Science my interests networking resource optimisation, Business Intelligence Applications and other computer technologies. I came to Korea to study and have completed my masters at the prestigious [Kyung Hee University](https://www.khu.ac.kr/eng/main/index.do) in South Korea. After graduation my goal is to work in an company that is developing solutions now for the future and require my expertise to promote innovation.

## Work Experience

| Network Operations Engineer | [Spark NZ](https://www.sparknz.co.nz/)  | Feb 2016 to Feb 2017 |
| ------------- |:-------------:| -----:|
- Ensure the entire company is kept up to date with major and minor network impacting events in New Zealand
- Work within a team of over 100 people to ensure service to millions of nationwide customers
- Manage service impacting events, proactive and reactive.
- Assist special events requiring extra resources including cell towers and mobile base stations.
- Assist roaming activities to keep our overseas partners visiting customers in coverage. 

|  Associate Business Analyst   | [Methanex NZ](https://methanex.com/)  | Nov 2015 to Feb 2016 |
| ------------- |:-------------:| -----:|
- Ensure the entire company is kept up to date with major and minor network impacting events in New Zealand
- Work within a team of over 100 people to ensure service to millions of nationwide customers
- Manage service impacting events, proactive and reactive.
- Assist special events requiring extra resources including cell towers and mobile base stations.
- Assist roaming activities to keep our overseas partners visiting customers in coverage. 

| English Conversation Tutoring | [Italki](https://www.italki.com)  | Feb 2011 - Feb 2014 |
| ------------- |:-------------:| -----:|
- Teaching English to students through Skype.
- Free talking, grammar advice, and even providing travel ideas around New Zealand.
- Teach English effectively by using interesting topics, cultural ideas related to English

| Signwriter | [Zodiac Signs Ltd](https://zodiacsigns.co.nz/)  | Feb 2011 to Feb 2014 |
| ------------- |:-------------:| -----:|
- Conferring with clients and responding to proposals, sketches and written instructions to determine composition of signs
- Designing and creating signs and graphics using computer software and sign making machines
- Preparing cost estimates for labour and materials
- Installing signs on-site

## Academic History

| MSc Computer Science | [Kyung Hee University](https://www.khu.ac.kr/eng/main/index.do)  | Mar 2017 - Feb 2019 |
| ------------- |:-------------:| -----:|
- [Published SCI(E) Journal “Lynskey, J.; Thar, K.; Oo, T.Z.; Hong, C.S. Facility Location Problem Approach for Distributed Drones. Symmetry 2019, 11, 118.”](https://www.mdpi.com/2073-8994/11/1/118)

| BSc Computer Science | [The University of Waikato](https://www.waikato.ac.nz/)  | Feb 2013 - Nov 2016 |
| ------------- |:-------------:| -----:|
- Best new Club in 2013, Director of Social Media, [Waikato Musicians Club (2013)](https://www.facebook.com/waikatomusiciansclub/).

## Responsiblities, Achievements,  and  Awards
- Kyung Hee University Scholarship Recipient (2016)
- [Upper-intermediate Korean Ability (TOPIK Level 4)](http://www.topik.go.kr/usr/cmm/index.do)
- [One SCI(E) published journal](https://www.mdpi.com/2073-8994/11/1/118)
- Bronze Young NZ Challenge (2011)
- New Zealand finalist at the Quiz on Korea (2014)
- [St John Youth](https://youth.stjohn.org.nz/), Grand Prior Award (2010)

## Professional Skills
- Able to develop relationships with people from all levels of social and cultural backgrounds.
- Programming - C#, Python primarily for Web Development and optimization.
- Positive working attitude within teams.
- Synthesizing large amounts of data into meaningful actions
- Previous professional experience in high profile international and national companies including [Methanex NZ](https://methanex.com/) and [Spark NZ](https://www.sparknz.co.nz/).